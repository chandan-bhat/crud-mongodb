package com.chandan.crudmongo.config;

import com.chandan.crudmongo.model.document.Users;
import com.chandan.crudmongo.repository.UserRepository;
import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

@Configuration
@EnableMongoRepositories(basePackageClasses = UserRepository.class)
public class MongoDbConfig extends AbstractMongoClientConfiguration {

	/**
	 * Utility function that generates a hashed password with salt
	 * @param password
	 * @return String
	 */
	private String passwordHasherUtil(String password) {
		
		int strength = 10; 
		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(strength, new SecureRandom());
		return bCryptPasswordEncoder.encode(password);
	}
	
	@Bean
	CommandLineRunner commandLineRunner (UserRepository userRepository) {
		return strings -> {
			
			userRepository.deleteAll();

			ArrayList<Long> contactNumbers = new ArrayList<Long>();
			contactNumbers.add(9632916454L);
			contactNumbers.add(9242013005L);

			userRepository.save(new Users("101","Chandan","Bhat",21,"Address","img1.jpeg", passwordHasherUtil("chandan"),contactNumbers));

			contactNumbers.set(0,8660548010L);
			contactNumbers.set(1,9964405858L);
			userRepository.save(new Users("102","Harshitha","Bhat", 18, "Address", "img2.jpeg", passwordHasherUtil("harshitha"), contactNumbers));
		};
	}


	@Override
	protected String getDatabaseName() {
		return "mongo";
	}

	@Override
	public MongoClient mongoClient() {
		ConnectionString connectionString = new ConnectionString("mongodb://localhost:27017/mongo");
		MongoClientSettings mongoClientSettings = MongoClientSettings.builder()
				.applyConnectionString(connectionString)
				.build();

		return MongoClients.create(mongoClientSettings);
	}

	@Override
	public Collection getMappingBasePackages() {
		return Collections.singleton("com.chandan");
	}
}

