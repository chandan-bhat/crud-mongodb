package com.chandan.crudmongo.controller;

import com.chandan.crudmongo.model.document.Users;
import com.chandan.crudmongo.exception.CustomException;
import com.chandan.crudmongo.model.pojo.EmptyJson;
import com.chandan.crudmongo.model.pojo.Info;
import com.chandan.crudmongo.services.UserService;
import com.chandan.crudmongo.utility.CrudUtility;
import com.chandan.crudmongo.utility.FileUploadUtility;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.chandan.crudmongo.utility.FileUploadUtility.saveFile;

@Slf4j
@RestController
@RequestMapping("/crud")
public class CrudController {

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	@Autowired
	private CrudUtility crudUtility;
	@Autowired
	private FileUploadUtility fileUploadUtility;

	@Autowired
	private UserService userService;


	/**
	 * API that returns all the users in the database
	 * @return Info(List<Users>)
	 */
	@GetMapping("/users/all")
	public ResponseEntity<Info> displayAllUsers(){
		log.info("Request made to get all Users");

		List<Users> users= userService.getAllUsers();
		Info info = new Info("/users/all", "success", 200, users);

		return ResponseEntity.status(HttpStatus.OK).body(info);
	}
	
	
	/**
	 * API that returns the User Details given the username
	 * @param username
	 * @return Info(Users)
	 */
	@GetMapping("/users")
	public ResponseEntity<Info> displayUser(@RequestParam(value = "username") String username) {

		log.info(String.format("Request made to get details of user with username %s", username));

		Users user = null;

		if(crudUtility.checkUserUtil(username)) {
			user = userService.getUser(username);
		}

		HttpStatus statusCode = null;
		Info info = null;

		// (user is null) if user name not found, Returning status code 400 (Bad Request)
		if(user == null){
			log.info("User with username " + username + " not found");

			info = new Info("/crud/users", "User Not Found", 404, new EmptyJson());
			statusCode = HttpStatus.NOT_FOUND;
		}
		else {
			log.info("User with username " + username + " found");

			info = new Info("/crud/users", "User Found", 200, user);
			statusCode = HttpStatus.OK;
		}
		return ResponseEntity.status(statusCode).body(info);
	}


	/**
	 * API checks if the user name is present or not present
	 * @param chkUsername
	 * @return ResponseEntity<String>
	 */
	@GetMapping(path = "/check/{username}", produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<Info> checkUser(@PathVariable(value="username") String chkUsername) {

		String usernameToBeChecked = chkUsername;

		Info info = null;
		if(crudUtility.checkUserUtil(usernameToBeChecked)) {
			Users user = userService.getUser(usernameToBeChecked);
			info = new Info("/check/"+chkUsername,"Username Present",200, user);
		}
		else {
			info = new Info("/check/"+chkUsername, "Username Not Present", 404, new EmptyJson());
		}
		return ResponseEntity.status(HttpStatus.OK).body(info);
	}


	/**
	 *
	 * @param userJsonString
	 * @return Info
	 */
	@PostMapping(path = "/users/add", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<Info> addUser(@RequestBody String userJsonString) {

		JSONObject userJson = null;

		try {
			userJson = new JSONObject(userJsonString);

			String username = userJson.getString("username");
			String firstName = userJson.getString("firstName");
			String lastName = userJson.getString("lastName");
			int age = userJson.getInt("age");
			String address = userJson.getString("address");
			String image = userJson.getString("image");
			String password = userJson.getString("pswd");
			String pswdHash = crudUtility.passwordHasherUtil(password);
			JSONArray ctemp = userJson.getJSONArray("contact");
			ArrayList<Long> contactNumbers = new ArrayList<Long>();

			for(int i=0 ; i<ctemp.length() ; i++){
				contactNumbers.add(ctemp.getLong(i));
			}

			if(crudUtility.checkUserUtil(username)) {
				log.info("Username " + username + " already present in database");
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Info("/crud/users/add", "Username already exists", 400, new EmptyJson()));
			}

			else {
				// Add User to DB
				Users savedUser = userService.saveUser(new Users(username, firstName, lastName, age, address, image, pswdHash, contactNumbers));
				log.info("New user " + savedUser + " successfully added to the database");
				return ResponseEntity.status(HttpStatus.CREATED).body(new Info("/crud/users/add" ,"User Registered Successfully", 201, savedUser));
			}
		}

		catch (JSONException err) {
			log.info("JSON Parsing failed.");
			log.error(err.toString());
//			return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(new Info("/crud/users/add", "User Registration Failed", 400, new EmptyJson()));
			throw new CustomException(err.getMessage(), "/crud/users/add");
		}
	}


	/**
	 *
	 * @param delUsername
	 * @return
	 */
	@DeleteMapping("/users/delete/{username}")
	public ResponseEntity<Info> deleteUser(@PathVariable(value="username") String delUsername){

		log.info("Request made to Delete User");

		try {

			if(crudUtility.checkUserUtil(delUsername)) {
				userService.deleteUser(delUsername);
				log.info("User deleted successfully");
				return ResponseEntity.status(HttpStatus.OK).body(new Info("/crud/users/delete/" + delUsername, "User Delete Successful", 200, new EmptyJson()));
			}
			else {
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Info("/crud/users/delete/" + delUsername, "User Delete Failed due to Invalid username", 404, new EmptyJson()));
			}
		}

		catch(Exception err){
			log.info("User Delete failed");
			log.error(err.toString());
//			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Info("/crud/users/delete/"+delUsername, "User Delete Failed", 400, new EmptyJson()));
			throw new CustomException("User Delete Failed","/crud/users/delete/"+delUsername);
		}
	}


	/**
	 * API that authenticates the username and password
	 * @param jsonBodyString
	 * @return Info
	 */
	@PostMapping(path="/login", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<Info> register(@RequestBody String jsonBodyString){

		log.info("Request made to Login");

		JSONObject jsonBody = null;
		String uname = null, pswd = null;
		try {
			jsonBody = new JSONObject(jsonBodyString);
			uname = jsonBody.getString("username");
			pswd = jsonBody.getString("password");
		}
		catch(JSONException err) {
			log.error(err.toString());
//			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Info("/crud/login", "Login Failed", 400, new EmptyJson()));
			throw new CustomException("Login Failed", "/crud/login");
		}

		if(crudUtility.checkUserUtil(uname)) {

			Users user = userService.getUser(uname);

			if(bCryptPasswordEncoder.matches(pswd, user.getPswdHash())) {
				log.info("Username and password hash match");
				return ResponseEntity.status(HttpStatus.OK).body(new Info("/crud/login", "Login Successful", 200, new EmptyJson()));
			}
			else {
				log.info("Incorrect Password for username " + uname);
				return ResponseEntity.status(HttpStatus.OK).body(new Info("/crud/login", "Login Unsuccessful, Incorrect password", 400, new EmptyJson()));
			}
		}
		else {
			log.info("Username " + uname + " not found");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Info("/crud/login", "Invalid Username", 404, new EmptyJson()));
		}
	}

	/**
	 *
	 * @param jsonBodyString
	 * @return
	 */
	@PutMapping("/update/password")
	public ResponseEntity<Info> changePassword(@RequestBody String jsonBodyString){

		log.info("Request made to update user password");

		JSONObject jsonBody = null;
		String username = null, newPassword = null, oldPassword = null;
		try {
			jsonBody = new JSONObject(jsonBodyString);
			username = jsonBody.getString("username");
			newPassword = jsonBody.getString("newPswd");
			oldPassword = jsonBody.getString("oldPswd");
		}

		catch (JSONException e) {
			log.error(e.toString());
			log.info("Password Update failed!");
//			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Info("/crud/update/password", "Operation failed", 400, new EmptyJson()));
			throw new CustomException("Operation failed", "/crud/update/password");
		}

		if(userService.checkUser(username) && bCryptPasswordEncoder.matches(oldPassword, userService.getUser(username).getPswdHash())) {
			// Valid Username and old password matches
			Users toBeUpdatedUser = userService.getUser(username);
			toBeUpdatedUser.setPswdHash(bCryptPasswordEncoder.encode(newPassword));

			Users savedUser = userService.saveUser(toBeUpdatedUser);
			log.info("User password updated and persisted");

			return ResponseEntity.status(HttpStatus.OK).body(new Info("/crud/update/password", "Password Reset Successful", 200, savedUser));
		}

		else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Info("/crud/update/password", "Password Reset Failed, Invalid Credentials", 404, new EmptyJson()));
		}
	}


	/**
	 *
	 * @param jsonBodyString
	 * @return
	 */
	@PutMapping("/update/firstname")
	private ResponseEntity<Info> changeFirstName(@RequestBody String jsonBodyString){

		JSONObject jsonBody = null;
		String username = null, newFirstName = null;
		Info info = null;
		HttpStatus statusCode = null;

		try {
			jsonBody = new JSONObject(jsonBodyString);
			username = jsonBody.getString("username");
			newFirstName = jsonBody.getString("newFirstName");
		}

		catch(JSONException err){
			log.error(err.toString());
			throw new CustomException("Operation failed", "/update/firstname");
		}

		if(crudUtility.checkUserUtil(username)) {
			Users toBeUpdatedUser = userService.getUser(username);
			// updating firstname
			toBeUpdatedUser.setFirstName(newFirstName);
			Users savedUser = userService.saveUser(toBeUpdatedUser);
			info = new Info("/crud/update/firstname","Firstname Updated Successfully", 200, savedUser);
			statusCode = HttpStatus.OK;
		}
		else {
			info = new Info("/crud/update/firstname", "Firstname Update Failed", 400, new EmptyJson());
			statusCode = HttpStatus.BAD_REQUEST;
		}

		return ResponseEntity.status(statusCode).body(info);
	}


	/**
	 *
	 * @param jsonBodyString
	 * @return
	 */
	@PutMapping("/update/lastname")
	private ResponseEntity<Info> changeLastName(@RequestBody String jsonBodyString){

		JSONObject jsonBody;
		String username = null, newLastName = null;
		Info info = null;
		HttpStatus statusCode = null;

		try {
			jsonBody = new JSONObject(jsonBodyString);
			username = jsonBody.getString("username");
			newLastName = jsonBody.getString("newLastName");
		}

		catch(JSONException err){
			log.error(err.toString());
			throw new CustomException("Operation Failed", "/update/lastname");
		}

		if(crudUtility.checkUserUtil(username)) {
			Users toBeUpdatedUser = userService.getUser(username);
			// updating lastname
			toBeUpdatedUser.setLastName(newLastName);
			try {
				Users savedUser = userService.saveUser(toBeUpdatedUser);
				info = new Info("/crud/update/lastname","Lastname Updated Successfully", 200, savedUser);
				statusCode = HttpStatus.OK;
			}
			catch(Exception err){
				log.info("Update Operation failed");
			}


		}
		else {
			info = new Info("/crud/update/lastname","Lastname Update Failed", 400, new EmptyJson());
			statusCode = HttpStatus.BAD_REQUEST;
		}

		return ResponseEntity.status(statusCode).body(info);
	}


	/**
	 *
	 * @return
	 */
	@PostMapping(value="/password/strength")
	public ResponseEntity<Info> checkPasswordStrength(@RequestBody String password){

		log.info("Request made to check password strength");

		long passwordLength = password.length();
		boolean hasUpper = false, hasLower = false, hasNum = false;

		for(int i=0 ; i < passwordLength ; i++){
			char c = password.charAt(i);
			if(Character.isLowerCase(c))
				hasLower = true;
			else if(Character.isUpperCase(c))
				hasUpper = true;
			else if(Character.isDigit(c))
				hasNum = true;
		}

		Info info = null;
		if(hasLower && hasNum && hasUpper && passwordLength > 8){
			info = new Info("/password/strength", "Strong", 200, new EmptyJson());
		}
		else if((hasLower && hasUpper) || (passwordLength > 8)){
			info = new Info("/password/strength", "Moderate", 200, new EmptyJson());
		}
		else{
			info = new Info("/password/strength", "Weak", 200, new EmptyJson());
		}

		return ResponseEntity.status(HttpStatus.OK).body(info);
	}


	@PostMapping("/{username}/upload")
	public ResponseEntity<Info> uploadImage(@RequestParam("image") MultipartFile multipartFile, @PathVariable("username") String username){

		log.info("Request made to upload image");

		if(crudUtility.checkUserUtil(username)){
			log.info("Valid User");
			String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
			Users toBeUpdatedUser = userService.getUser(username);

			toBeUpdatedUser.setImage(fileName);
			String uploadDir = "user-photos/" + toBeUpdatedUser.getUsername();

			try {
				saveFile(uploadDir, fileName, multipartFile);
				log.info("Image Successfully saved on server");
				// Updating The database only if upload is successful
				Users savedUser = userService.saveUser(toBeUpdatedUser);
				return ResponseEntity.status(HttpStatus.OK).body(new Info("/crud/"+username+"/upload", "Image Uploaded Successfully", 200, savedUser));

			} catch (IOException e) {
				log.info("Image Upload Failed");
				log.error(e.toString());
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Info("/crud/"+username+"/upload", "Image Upload Failed", 400, new EmptyJson()));
			}
		}
		else{
			log.error("Invalid Username");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Info("/crud/"+username+"/upload", "Image Upload Failed, Invalid username", 404, new EmptyJson()));
		}
	}


	@GetMapping("/search")
	public ResponseEntity<Info> searchByContactNumber(@RequestParam("contact") Long contactNumber){

		List<Users> users = userService.getUserByContactNumber(contactNumber);
		return ResponseEntity.status(HttpStatus.OK).body(new Info("/crud/search", "Search by Contact Number", 200, users));
	}


	@GetMapping("/test")
	public void message() {
		try{
			throw new CustomException("abcde","/test");
		}
		catch(CustomException e){

			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,e.getMessage(),e);
		}
	}

	@Autowired
	MongoTemplate mongoTemplate;

	@GetMapping("/testMongo")
	public List<Users> getNewUser() {
		Query query = new Query(Criteria.where("age").gte(19));
		return mongoTemplate.find(query, Users.class, "myUsers");
	}
}


