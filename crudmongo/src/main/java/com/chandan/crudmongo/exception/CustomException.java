package com.chandan.crudmongo.exception;

public class CustomException extends RuntimeException {

    private String path;

    public CustomException(String message, String path){
        super(message);
        this.path = path;
    }

    public String getPath() {
        return path;
    }
}
