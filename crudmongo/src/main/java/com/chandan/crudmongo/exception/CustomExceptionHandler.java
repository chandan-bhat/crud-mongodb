package com.chandan.crudmongo.exception;

import com.chandan.crudmongo.model.pojo.EmptyJson;
import com.chandan.crudmongo.model.pojo.Info;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class CustomExceptionHandler {

    @ExceptionHandler(value={CustomException.class})
    public ResponseEntity<Info> errorResponse(CustomException e){
        return new ResponseEntity<Info>(new Info(e.getPath(), e.getMessage(), 400, new EmptyJson()), HttpStatus.BAD_REQUEST);
    }
}
