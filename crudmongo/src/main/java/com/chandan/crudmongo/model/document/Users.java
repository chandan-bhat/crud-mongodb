package com.chandan.crudmongo.model.document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;

@Document(collection = "myUsers")
@Data @AllArgsConstructor @NoArgsConstructor
public class Users {

	@Id
	private String username;
	private String firstName;
	private String lastName;
	private int age;
	private String address;
	private String image;
	private String pswdHash;
	private ArrayList<Long> contactNumbers;
}
