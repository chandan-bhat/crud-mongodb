package com.chandan.crudmongo.model.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

@Data
@NoArgsConstructor @AllArgsConstructor
public class Info {

	@Id
	private String api;
	private String message;
	private int status;
	private Object content;
}
