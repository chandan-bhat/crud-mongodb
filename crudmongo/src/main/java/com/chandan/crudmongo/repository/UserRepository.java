package com.chandan.crudmongo.repository;

import com.chandan.crudmongo.model.document.Users;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;


public interface UserRepository extends MongoRepository<Users,String> {

	public Users findByUsername(String username);

	@Query(value= "{contactNumbers: ?0 }")
	public List<Users> findByContactNumbers(Long number);
}
