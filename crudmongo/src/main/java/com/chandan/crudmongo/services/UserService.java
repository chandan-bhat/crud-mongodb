package com.chandan.crudmongo.services;

import com.chandan.crudmongo.model.document.Users;
import com.chandan.crudmongo.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public List<Users> getAllUsers(){
        return userRepository.findAll();
    }

    public Users getUser(String username){
        return userRepository.findByUsername(username);
    }

    public Users saveUser(Users user){
        // Return type of save() method is the entity, Users in this case
        return userRepository.save(user);
    }

    public void deleteUser(String username){
        // Delete User by Id (username)
        userRepository.deleteById(username);
    }

    public Boolean checkUser(String username){
        return userRepository.existsById(username);
    }

    public List<Users> getUserByContactNumber(Long contactNumber){
        return userRepository.findByContactNumbers(contactNumber);
    }
}
