package com.chandan.crudmongo.utility;

import com.chandan.crudmongo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class CrudUtility {

    @Autowired
    UserRepository userRepository;
    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    /**
     * Utility function that returns true if username exists and false otherwise
     * @param usernameToBeChecked
     * @return Boolean
     *
     */
    public Boolean checkUserUtil(String usernameToBeChecked) {
        return userRepository.existsById(usernameToBeChecked);
    }

    /**
     * Utility function that generates a hashed password with salt
     * @param password
     * @return String
     */
    public String passwordHasherUtil(String password) {
        return bCryptPasswordEncoder.encode(password);
    }
}
